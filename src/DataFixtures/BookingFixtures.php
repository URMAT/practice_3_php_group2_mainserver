<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Cottage;
use App\Entity\Pension;
use App\Entity\User;
use App\Model\Client\ClientHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BookingFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $bookingObject1 = new Cottage();
        $bookingObject1->setNameObject('Голубой Иссык Куль');
        $bookingObject1->setAddress('some address');
        $bookingObject1->setFullName('Иваницина Бактыгул Марковка');
        $bookingObject1->setQuantityRoom(30);
        $bookingObject1->setPrice(1000);
        $bookingObject1->setContactPhone(+996123123456);
        $bookingObject1->setKitchen(true)
                    ->setSpa(false)
                    ->setInternet(false)
        ;


        $manager->persist($bookingObject1);

        $bookingObject2 = new Cottage();
        $bookingObject2->setNameObject('Акапулько');
        $bookingObject2->setAddress('some address');
        $bookingObject2->setFullName('Иваницина Бактыгул Марковка');
        $bookingObject2->setQuantityRoom(20);
        $bookingObject2->setPrice(2000);
        $bookingObject2->setContactPhone(+996123123456);
        $bookingObject2->setKitchen(true)
            ->setSpa(false)
            ->setInternet(true)
        ;

        $manager->persist($bookingObject2);

        $bookingObject3 = new Pension();
        $bookingObject3->setNameObject('Золотые пески');
        $bookingObject3->setAddress('some address 1');
        $bookingObject3->setFullName('Пушкин Александр Сергеевич');
        $bookingObject3->setQuantityRoom(50);
        $bookingObject3->setPrice(1500);
        $bookingObject3->setContactPhone(+996123123456);
        $bookingObject3->setCook(true)
                    ->setSwimmingPool(false)
                    ->setTv(true)
        ;

        $manager->persist($bookingObject3);

        $bookingObject4 = new Pension();
        $bookingObject4->setNameObject('Калипсо');
        $bookingObject4->setAddress('some address 1');
        $bookingObject4->setFullName('Лермонтов Михаил Юрьевич');
        $bookingObject4->setQuantityRoom(100);
        $bookingObject4->setPrice(1500);
        $bookingObject4->setContactPhone(+996123123456);
        $bookingObject4->setCook(false)
            ->setSwimmingPool(true)
            ->setTv(true)
        ;

        $manager->persist($bookingObject4);

        $manager->flush();
    }
}

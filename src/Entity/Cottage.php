<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity
 */
class Cottage extends BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $kitchen;

    /**
     * @ORM\Column(type="boolean")
     */
    private $spa;

    /**
     * @ORM\Column(type="boolean")
     */
    private $internet;

    /**
     * @return array
     */
    public function __toArray(){
        $cottage = [];
        $cottage['kitchen'] = $this->getKitchen();
        $cottage['address'] = $this->getAddress();
        $cottage['full_name'] = $this->getFullName();
        $cottage['contact_phone'] = $this->getContactPhone();
        $cottage['price'] = $this->getPrice();
        $cottage['name_object'] = $this->getNameObject();
        $cottage['internet'] = $this->getInternet();
        $cottage['spa'] = $this->getSpa();
        $cottage['quantity_room'] = $this->getQuantityRoom();
        return $cottage;
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $kitchen
     * @return Cottage
     */
    public function setKitchen($kitchen)
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->kitchen;
    }

    /**
     * @param mixed $spa
     * @return Cottage
     */
    public function setSpa($spa)
    {
        $this->spa = $spa;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpa()
    {
        return $this->spa;
    }

    /**
     * @param mixed $internet
     * @return Cottage
     */
    public function setInternet($internet)
    {
        $this->internet = $internet;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInternet()
    {
        return $this->internet;
    }


}

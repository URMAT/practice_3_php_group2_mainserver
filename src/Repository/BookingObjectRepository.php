<?php

namespace App\Repository;

use App\Entity\BookingObject;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;
use function Symfony\Component\DependencyInjection\Loader\Configurator\expr;


/**
 * @method BookingObject|null find($id, $lockMode = null, $lockVersion = null)
 * @method BookingObject|null findOneBy(array $criteria, array $orderBy = null)
 * @method BookingObject[]    findAll()
 * @method BookingObject[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingObjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BookingObject::class);
    }

    public function sortObject(array $data)
    {

            $qb = $this->createQueryBuilder('a');
                $qb->select('a')
                ->where($qb->expr()->like('a.name_object', ':word'))
                ->setParameter('word', '%'. $data['word'] .'%')
                ->andWhere('a.price BETWEEN :min_price AND :max_price')
                ->setParameter('min_price', $data['min_price'] ?? 0)
                ->setParameter('max_price', $data['max_price'] ?? 10000000)
                ;

            return $qb->getQuery()
                ->getResult();

    }
}

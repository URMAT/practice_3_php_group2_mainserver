<?php

namespace App\Repository;

use App\Entity\Cottage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method Cottage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cottage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cottage[]    findAll()
 * @method Cottage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CottageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cottage::class);
    }

    public function sortObject(array $data)
    {
            $qb = $this->createQueryBuilder('a');
                $qb->select('a')
                ->where($qb->expr()->like('a.name_object', ':word'))
                ->setParameter('word', '%'. $data['word'] .'%')
                ->andWhere('a.price BETWEEN :min_price AND :max_price')
                ->setParameter('min_price', $data['min_price'] ?? 0)
                ->setParameter('max_price', $data['max_price'] ?? 10000000)
                ;

            return $qb
                    ->getQuery()
                    ->getResult();

    }
}

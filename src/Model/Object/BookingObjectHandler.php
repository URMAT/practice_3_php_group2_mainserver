<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\Object;

use App\Entity\Cottage;
use App\Entity\Pension;

class BookingObjectHandler
{
    public function createNewPension(array $data){
        $pension = new Pension();
        $pension->setNameObject($data['name_object'])
            ->setFullName($data['full_name'])
            ->setContactPhone($data['contact_phone'])
            ->setQuantityRoom($data['quantity_room'])
            ->setPrice($data['price'])
            ->setAddress($data['address'])
            ->setCook($data['cook'])
            ->setTv($data['tv'])
            ->setSwimmingPool($data['swimming_pool'])
            ;
        return $pension;
    }

    public function createNewCottage(array $data){
        $cottage = new Cottage();
        $cottage->setNameObject($data['name_object'])
            ->setFullName($data['full_name'])
            ->setContactPhone($data['contact_phone'])
            ->setQuantityRoom($data['quantity_room'])
            ->setPrice($data['price'])
            ->setAddress($data['address'])
            ->setKitchen($data['kitchen'])
            ->setInternet($data['internet'])
            ->setSpa($data['spa'])
            ;
        return $cottage;
    }
}

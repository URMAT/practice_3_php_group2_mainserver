<?php

namespace App\Controller;

use App\Model\Client\ClientHandler;
use App\Model\Object\BookingObjectHandler;
use App\Repository\BookingObjectRepository;
use App\Repository\ClientRepository;
use App\Repository\CottageRepository;
use App\Repository\PensionRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportOrEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/check_client_credentials/{encodedPassword}/{email}", name="app_check_client_credentials")
     * @Method("HEAD")
     * @param string $encodedPassword
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function checkClientCredentialsAction(
        string $encodedPassword,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($encodedPassword, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['vkId'] = $request->request->get('vkId');
        $data['faceBookId'] = $request->request->get('faceBookId');
        $data['googleId'] = $request->request->get('googleId');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportOrEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/registration_pension", name="app_registration_pension")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function registrationPensionAction(
        BookingObjectHandler $objectHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['name_object'] = $request->request->get('name_object');
        $data['full_name'] = $request->request->get('full_name');
        $data['contact_phone'] = $request->request->get('contact_phone');
        $data['quantity_room'] = $request->request->get('quantity_room');
        $data['price'] = $request->request->get('price');
        $data['address'] = $request->request->get('address');
        $data['cook'] = $request->request->get('cook');
        $data['tv'] = $request->request->get('tv');
        $data['swimming_pool'] = $request->request->get('swimming_pool');


        $pension = $objectHandler->createNewPension($data);

        $manager->persist($pension);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/registration_cottage", name="app_registration_cottage")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function registrationCottageAction(
        BookingObjectHandler $objectHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['name_object'] = $request->request->get('name_object');
        $data['full_name'] = $request->request->get('full_name');
        $data['contact_phone'] = $request->request->get('contact_phone');
        $data['quantity_room'] = $request->request->get('quantity_room');
        $data['price'] = $request->request->get('price');
        $data['address'] = $request->request->get('address');
        $data['kitchen'] = $request->request->get('kitchen');
        $data['internet'] = $request->request->get('internet');
        $data['spa'] = $request->request->get('spa');


        $pension = $objectHandler->createNewCottage($data);

        $manager->persist($pension);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/client/email/{email}", name="app_client_by_email")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientByEmailAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $user = $clientRepository->findOneByEmail($email);
        if ($user) {
            return new JsonResponse($user->__toArray());
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/take_booking_object", name="app_take_booking_object")
     * @Method("GET")
     */
    public function allBookingObjectAction()
    {
        $objects = [];
        $allBookingObject = $this
            ->getDoctrine()
            ->getRepository('App:BookingObject')
            ->findAll();

        foreach ($allBookingObject as $object){
            $array = $object->__toArray();
            $array ['entity_name'] = get_class($object);
            $objects[] = $array;
        }

        return new JsonResponse($objects);
    }

    /**
     * @Route("/find_by_word", name="app_find_by_word")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function findByWordAction(
        Request $request,
        BookingObjectRepository $bookingObjectRepository,
        PensionRepository $pensionRepository,
        CottageRepository $cottageRepository
    )
    {
        $data = [];
        $data ['word'] = $request->query->get('word');
        $data ['min_price'] = $request->query->get('min_price');
        $data ['max_price'] = $request->query->get('max_price');
        $data ['kind_object'] = $request->query->get('kind_object');

        switch ($data['kind_object'])
        {
            case 'pension':
                $bookingObject = $pensionRepository->sortObject($data);
                break;
            case 'Cottage':
                $bookingObject = $cottageRepository->sortObject($data);
                break;
            default:
                $bookingObject = $bookingObjectRepository->sortObject($data);
        }



        if($bookingObject){
            $bookingObjectArray = [];
            foreach ($bookingObject as $object){
                $Array = $object->__toArray();
                $Array ['entity_name'] = get_class($object);
                $bookingObjectArray [] = $Array;
            }
        }

        if ($bookingObject) {
            return new JsonResponse($bookingObjectArray);
        }
        else {
            throw new NotFoundHttpException();
        }
    }
    /**
     * @Route("/sort_min_or_max", name="app_sort_min_or_max")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function sortByMinOrMaxAction(
        Request $request,
        BookingObjectRepository $bookingObjectRepository,
        LoggerInterface $logger
    )
    {
        $minOrMax = $request->request->get('kind_filter');
//        $logger->log('INFO', $request->query->get('word'));

//        if($minOrMax == 'by_min'){
            $BookingObject = $bookingObjectRepository->sortByMin();
//        }

        $array = $BookingObject->__toArray();
        $array ['entity_name'] = get_class($BookingObject);
        $objects[] = $array;

        if ($BookingObject) {
            return new JsonResponse($objects);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/add_to_client_social_network")
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param ObjectManager $manager
     */
    public function addSocialAction(
        Request $request,
        ClientRepository $clientRepository,
        ObjectManager $manager
    )
    {
        $emailFromLocalServer = $request->query->get('email_user');
        $network = $request->query->get('network');
        $uid = $request->query->get('uid');
        $resultSql = $clientRepository->findOneByEmail($emailFromLocalServer);

        $resultSql->setSocialId($network, $uid);
        $manager->persist($resultSql);
        $manager->flush();

        $this->render('base.html.twig');
    }
}
